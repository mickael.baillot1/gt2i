<?php
// Import lib(s)
require('MagicParser.php');
require('../config/settings.inc.php');

// Configuration
$dsn = 'mysql:dbname='._DB_NAME_.';host=127.0.0.1';
$user = _DB_USER_;
$password = _DB_PASSWD_;

// Display errors
ini_set('display_errors', 'on');
error_reporting(E_ALL | E_STRICT);

$dbh = new PDO($dsn, $user, $password);
$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

$errors = array();
/*

Example data product XML et commentaires

<PRODUIT_POCLEUNIK>1</PRODUIT_POCLEUNIK> // OK id du produit
<PRODUIT_REF>M-40106</PRODUIT_REF> // Ok product_ref
<REFCIALE_ARCLEUNIK>1</REFCIALE_ARCLEUNIK>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<REFCIALE_REFART>M-40106</REFCIALE_REFART>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<REFCIALE_REFCAT>M-40106</REFCIALE_REFCAT>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<POTRAD_DESI>Pompe FACET TRANSISTORISEE</POTRAD_DESI> // Ok nom produit
<REFCIALE_CTVA>2</REFCIALE_CTVA>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<FICTECH_MEMOCAT>Pompe Facet en 12V (3/4 ampères). La pompe Facet transistorisée est auto régulée, étanche et légère.  Attention!!! Le filtre de la pompe à essence n&apos;est pas inclus.</FICTECH_MEMOCAT> // OK => description produits
<FICTECH_MEMONET></FICTECH_MEMONET>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_MARQUE>GL</PRODUIT_MARQUE>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP01>MO</PRODUIT_CLEP01>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP02>ALE</PRODUIT_CLEP02>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP03>AIW</PRODUIT_CLEP03>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP04>O</PRODUIT_CLEP04>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP06>N</PRODUIT_CLEP06>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP07>O</PRODUIT_CLEP07>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_GCOLORIS></PRODUIT_GCOLORIS>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_GTAILLE></PRODUIT_GTAILLE>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_CLEP12></PRODUIT_CLEP12>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<REFCIALE_FICHEINA>0</REFCIALE_FICHEINA>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<REFCIALE_MODTE>20160411</REFCIALE_MODTE>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<PRODUIT_MODTE>20110523</PRODUIT_MODTE>  // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<ARTICLE_POIDS>0,534</ARTICLE_POIDS> // OK product_weight
<ARTICLE_HNORMEL>0</ARTICLE_HNORMEL> // Création d'une table auxiliaire qui va stocker ces données avec une ref au product_id
<ARTICLE_CATEG>1</ARTICLE_CATEG> // On a un id de catégorie sans nom donc je lie à la catégorie 1 bêtement pour l'instant, il faudrait un second XMl avec les catégories

*/


function saveRecord($record) {
    // var_dump($record);
    // echo('<br/>
    // <br/>
    //
    // </br/>
    // </br/>------------/br/>
    // </br/>/br/>
    // </br/>/br/>
    // </br/>');

    global $dbh;
    global $errors;
    $lang = getIdLang();

    // On considère que l'import est initial au développement du projet donc on vide les tables produits et produits_lang


    $sqlProduct = 'INSERT INTO '._DB_PREFIX_.'product (id_product, id_category_default, reference, weight, active)
    VALUES ( '.$record['PRODUIT_POCLEUNIK'].', '.$record['ARTICLE_CATEG'].', "'.$record['PRODUIT_REF'].'", "'.$record['ARTICLE_POIDS'].'", 1 )';
    $resProduct = $dbh->exec($sqlProduct);

    if($resProduct != 1) {
        $errors[] = "Une erreur est survenue pendant l'import du produit ".$record['PRODUIT_POCLEUNIK'];
    }

    $sqlProductLang = 'INSERT INTO '._DB_PREFIX_.'product_lang (id_product, id_shop, id_lang, description, link_rewrite, name, available_now)
    VALUES ( '.$record['PRODUIT_POCLEUNIK'].',1, '.$lang.', "'.$record['FICTECH_MEMOCAT'].'", "'.urlencode($record['POTRAD_DESI']).'", "'.$record['POTRAD_DESI'].'", "En stock" )';
    $resProductLang = $dbh->exec($sqlProductLang);
    if($resProductLang != 1) {
        $errors[] = "Une erreur est survenue pendant l'import des informations de langue du produit ".$record['PRODUIT_POCLEUNIK'];
    }
    $sqlProductInfos = 'INSERT INTO '._DB_PREFIX_.'product_infos
    VALUES ( '.$record['PRODUIT_POCLEUNIK'].',"'.$record['REFCIALE_ARCLEUNIK'].'", "'.$record['REFCIALE_REFART'].'", "'.$record['REFCIALE_REFCAT'].'", "'.$record['REFCIALE_CTVA'].'", "'.$record['FICTECH_MEMONET'].'", "'.$record['PRODUIT_MARQUE'].'", "'.$record['PRODUIT_CLEP01'].'", "'.$record['PRODUIT_CLEP02'].'","'.$record['PRODUIT_CLEP03'].'","'.$record['PRODUIT_CLEP04'].'", "'.$record['PRODUIT_CLEP06'].'", "'.$record['PRODUIT_CLEP07'].'", "'.$record['PRODUIT_GCOLORIS'].'", "'.$record['PRODUIT_GTAILLE'].'","'.$record['PRODUIT_CLEP12'].'","'.$record['REFCIALE_FICHEINA'].'", "'.$record['REFCIALE_MODTE'].'", "'.$record['PRODUIT_MODTE'].'", "'.$record['ARTICLE_HNORMEL'].'" )';

    $resProductInfos = $dbh->exec($sqlProductInfos);
    if($resProductInfos != 1) {
        $errors[] = "Une erreur est survenue pendant l'import des informations complémentaires du produit ".$record['PRODUIT_POCLEUNIK'];
    }
}

function getIdLang() {
    // Fixture Default Prestashop lang
    return 1;
    //End fixture
}


function createTable($dbh) {
    $sql ="CREATE table IF NOT EXISTS "._DB_PREFIX_."product_infos (
    `id_product` INT( 11 ) NOT NULL,
    `refciale_arcleunik` VARCHAR( 50 ) NULL NULL,
    `refciale_refart` VARCHAR( 50 ) NULL,
    `refciale_refcat` VARCHAR( 50 ) NULL,
    `refciale_ctva` VARCHAR( 50 ) NULL,
    `fictech_memonet` VARCHAR( 50 ) NULL,
    `produit_marque` VARCHAR( 50 ) NULL,
    `produit_clep01` VARCHAR( 50 ) NULL,
    `produit_clep02` VARCHAR( 50 ) NULL,
    `produit_clep03` VARCHAR( 50 ) NULL,
    `produit_clep04` VARCHAR( 50 ) NULL,
    `produit_clep06` VARCHAR( 50 ) NULL,
    `produit_clep07` VARCHAR( 50 ) NULL,
    `produit_gcoloris` VARCHAR( 50 ) NULL,
    `produit_gtaille` VARCHAR( 50 ) NULL,
    `produit_clep12` VARCHAR( 50 ) NULL,
    `refciale_ficheina` VARCHAR( 50 ) NULL,
    `refciale_modte` VARCHAR( 50 ) NULL,
    `produit_modte` VARCHAR( 50 ) NULL,
    `article_hnormel` VARCHAR( 50 ) NULL,
    PRIMARY KEY  (`id_product`)
    );" ;
    try {
        $createTable = $dbh->exec($sql);
        print("Created "._DB_PREFIX_."product_infos Table.\n");
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function emptyTable($dbh) {
    $sql ="TRUNCATE TABLE "._DB_PREFIX_."product_infos";
    $dbh->exec($sql);
}

try {
    $sql ="TRUNCATE TABLE "._DB_PREFIX_."product";
    $dbh->exec($sql);

    $sql ="TRUNCATE TABLE "._DB_PREFIX_."product_lang";
    $dbh->exec($sql);

    createTable($dbh);
    emptyTable($dbh);


    // Récupération de données
    /*$req = $dbh->query($sql);
    while($row = $req->fetch()) {

    }*/

    // Execution requpete mysql
    /*$sql = 'DELETE FROM accouns WHERE type = "member"';
    $prepare = $pdo->prepare($sql);
    $res = $prepare ->exec($sql);*/

    MagicParser_parse("catalogue.XML","saveRecord");

    if(count($errors) > 0) {
        foreach ($errors as $key => $error) {
            echo $error. '<br/>';
        }
    }

} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}



?>
