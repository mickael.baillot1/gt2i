<?php /*%%SmartyHeaderCode:20965190715a7c96e9eb78f2-56944418%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2979c9f867ac0a84d0b8a4a7f439e9a1031671f9' => 
    array (
      0 => '/var/www/html/prestashop-gt2i/themes/default-bootstrap/modules/blockcategories/blockcategories_footer.tpl',
      1 => 1517242832,
      2 => 'file',
    ),
    'fcec28fbe076788e12d719fb4c78b0104a406eff' => 
    array (
      0 => '/var/www/html/prestashop-gt2i/themes/default-bootstrap/modules/blockcategories/category-tree-branch.tpl',
      1 => 1517242832,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20965190715a7c96e9eb78f2-56944418',
  'variables' => 
  array (
    'isDhtml' => 0,
    'blockCategTree' => 0,
    'child' => 0,
    'numberColumn' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a7c96e9efb0c8_63902922',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a7c96e9efb0c8_63902922')) {function content_5a7c96e9efb0c8_63902922($_smarty_tpl) {?>
<!-- Block categories module -->
<section class="blockcategories_footer footer-block col-xs-12 col-sm-2">
	<h4>Catégories</h4>
	<div class="category_footer toggle-footer">
		<div class="list">
			<ul class="dhtml">
												
<li class="last">
	<a 
	href="http://dev.gt2i.com/index.php?id_category=3&amp;controller=category" title="Vous trouverez ici toutes les collections mode pour femmes.  
 Cette catégorie regroupe tous les basiques de votre garde-robe et bien plus encore : 
 chaussures, accessoires, T-shirts imprimés, robes élégantes et jeans pour femmes !">
		Femmes
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.gt2i.com/index.php?id_category=4&amp;controller=category" title="Choisissez parmi une large sélection de T-shirts à manches courtes, longues ou 3/4, de tops, de débardeurs, de chemisiers et bien plus encore. 
 Trouvez la coupe qui vous va le mieux !">
		Tops
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.gt2i.com/index.php?id_category=5&amp;controller=category" title="Les must have de votre garde-robe : découvrez les divers modèles ainsi que les différentes 
 coupes et couleurs de notre collection !">
		T-shirts
	</a>
	</li>

																
<li class="last">
	<a 
	href="http://dev.gt2i.com/index.php?id_category=7&amp;controller=category" title="Coordonnez vos accessoires à vos chemisiers préférés, pour un look parfait.">
		Chemisiers
	</a>
	</li>

									</ul>
	</li>

																
<li class="last">
	<a 
	href="http://dev.gt2i.com/index.php?id_category=8&amp;controller=category" title="Trouvez votre nouvelle pièce préférée parmi une large sélection de robes décontractées, d&#039;été et de soirée ! 
 Nous avons des robes pour tous les styles et toutes les occasions.">
		Robes
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.gt2i.com/index.php?id_category=9&amp;controller=category" title="Vous cherchez une robe pour la vie de tous les jours ? Découvrez 
 notre sélection de robes et trouvez celle qui vous convient.">
		Robes décontractées
	</a>
	</li>

																
<li >
	<a 
	href="http://dev.gt2i.com/index.php?id_category=10&amp;controller=category" title="Trouvez la robe parfaite pour une soirée inoubliable !">
		Robes de soirée
	</a>
	</li>

																
<li class="last">
	<a 
	href="http://dev.gt2i.com/index.php?id_category=11&amp;controller=category" title="Courte, longue, en soie ou imprimée, trouvez votre robe d&#039;été idéale !">
		Robes d&#039;été
	</a>
	</li>

									</ul>
	</li>

									</ul>
	</li>

							
										</ul>
		</div>
	</div> <!-- .category_footer -->
</section>
<!-- /Block categories module -->
<?php }} ?>
