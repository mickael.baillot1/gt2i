<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class QuantityNotifier extends Module
{


	public function __construct()
	{
		$this->name = 'quantitynotifier';
		$this->tab = 'administration';
		$this->version = '1.0.0';
		$this->author = 'mickael baillot';



		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Quantity notifier');
		$this->description = $this->l('Notify when quantity product is updated.');
		$this->confirmUninstall = $this->l('Are you sure you want to delete these details?');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');

	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('actionUpdateQuantity')) {


			return false;
		}

        // Add configs
        Configuration::updateValue('EMAIL_QUANTITY_NOTIFIER', 'mickael.baillot@gmail.com');

		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;

		return true;
	}

	public function hookActionUpdateQuantity($params)
	{
        // get mail to notify
        $mailtoNotify = Configuration::get('EMAIL_QUANTITY_NOTIFIER');

       // Vars to mails

       // TODO ajouter les params liés au produit
        $vars = array(
            '{email}' => $mailtoNotify,
        );

        $dateMail = date('d-m-Y');

        $id_lang = $this->context->language->id;
        $isSend = Mail::Send(
            (int)$id_lang,
            'notifier',
            Mail::l('Update quantity '.$dateMail, (int)$id_lang),
            $vars,
            $mailtoNotify,
            $mailtoNotify,
            null,
            null,
            null,
            null,
            _PS_MAIL_DIR_,
            false,
            (int)$this->context->shop->id
        );
	}

    private function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			Configuration::updateValue('EMAIL_QUANTITY_NOTIFIER', Tools::getValue('EMAIL_QUANTITY_NOTIFIER'));
		}
		$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}


	public function getContent()
	{
		$this->_html = '';

		if (Tools::isSubmit('btnSubmit'))
		{
				$this->_postProcess();
		}

		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Configuration quantity notifier'),
					'icon' => 'icon-person'
				),
                'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Email à notifier'),
						'name' => 'EMAIL_QUANTITY_NOTIFIER',
						'required' => true
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'btnSubmit';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'fields_value' => $this->getConfigFieldsValues()
		);

		return $helper->generateForm(array($fields_form));
	}

    public function getConfigFieldsValues()
	{
		return array(
			'EMAIL_QUANTITY_NOTIFIER' => Tools::getValue('EMAIL_QUANTITY_NOTIFIER', Configuration::get('EMAIL_QUANTITY_NOTIFIER'))
		);
	}
}
